var express = require('express');

//instantiating express object
var app= express();

var PORT=8081;

app.use(express.static(__dirname+'/../Client/')); //always executed
//console.log(__dirname+'/Client');

app.get('/about',function(req,res,next){
    console.log("entered the get of about");
    console.log("About ACME");
    next();
});

app.use('/about',function(req,res){
   console.log("entered the use of about");
    res.redirect('/index.html');
    console.log("This should not get printed if res.send works");
});

app.use(function(req,res){
    console.log("entered the use of 404");
   res.status(404).redirect('/404.html');
});

app.listen(PORT,function () {
    console.log("I am listening on port: "+ PORT );

});
