module.exports = function (app) {

    var fs = require("fs");
    var path = require("path");

    var multer = require("multer");

    var storage = multer.diskStorage({
        destination: __dirname + "/../client/images",
        filename: function (req, file, cb) {
            cb(null, file.originalname)
        }
    })

    var multipart = multer({storage: storage});

    app.post("/xxx", multipart.single("img-file"), function (req, res) {
        //TODO : insert into your session with Album Data Here
    });


    //TODO : write here the other routing to ADD and DELETE from Album

}
