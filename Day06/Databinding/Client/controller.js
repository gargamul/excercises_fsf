/**
 * Created by Amulya on 17/10/2016.
 */
var RegApp=angular.module("Apple",[]);

(function() {
    var RegCtrl = function () {
        var cope = this;
        cope.people = [
            {id: 1, last: "Clooney", first: "Roney"},
            {id: 2, last: "James", first: "lenon"},
            {id: 3, last: "Curie", first: "Marie"}
        ];

        cope.addPerson = function () {
            var newId = cope.people.length;
            newId++;

            cope.people.push({id: newId, last:cope.addLast, first:cope.addFirst});
        };


    };
    RegApp.controller('RegCtrl', [RegCtrl]);
})();