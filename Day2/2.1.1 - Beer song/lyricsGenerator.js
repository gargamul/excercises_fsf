/**
 * Created by Amulya on 11/10/2016.
 */
//98 bottles of beer on the wall, 98 bottles of beer.
    //Take one down and pass it around, 97 bottles of beer on the wall.
var i=99;

lyrics="";

var bottles= function getBottles(num)
{

    switch(num){
        case 1: return num+" bottle of beer";
        case i<0: return "No more bottles of beer";
        default: return num+" bottles of beer";

    }
}

do{
    console.log(bottles(i)+" on the wall, "+ bottles(i)+".");
    console.log(i>0?
        ("Take one down and pass it around, "+ bottles(i-1) + " on the wall.\n")
            :("Go to the store and buy some more, "+bottles(99)+" on the wall.\n"));

    --i;
}while(i>=0);

