/**
 * Created by Amulya on 12/10/2016.
 */
var Teslamodels=function (){
    this.numWheels=4;
    this.color="blue";
}

Teslamodels.prototype=function(){
    var go=function(){
        console.log("wheels rotating");
    }

    var stop=function(){
        console.log("stop rotating");
    }

    return{
        pressBrake: stop,
        pressGas:go
    }
}(); // take note of these closing brackets

module.exports=Teslamodels;