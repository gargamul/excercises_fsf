/**
 * Created by Amulya on 12/10/2016.
 */

function isOddOrEven(number){
    return promise2 = new Promise(function(resolve,reject){

            if (number % 2 == 1)
                resolve("Odd number");
            else if (number % 2 == 0)
                resolve("Even number");
            else
                reject("Invalid Input");
        });

}

isOddOrEven(20).then(function(Result){console.log(Result);}).catch(function(err){console.log(err);});
