(function() {
    angular
        .module("DepartmentApp")
        .controller("InsertCtrl", InsertCtrl);

    InsertCtrl.$inject = ["$http"];

    function InsertCtrl($http) {

        //to facilitate add department
        var vm=this;
        vm.dept={
            dept_no:0,
            dept_name:""
        };

        console.log("Department info got from html"+vm.dept);

        vm.register=function(){
            $http.post('/api/departments',vm.dept)
                .then(function(){
                    console.log("User was successfully registered i hope. Please check the database");
                })
                .catch(function(){
                    console.log("error during posting data to db :client");
                });
        };

    };
})();






