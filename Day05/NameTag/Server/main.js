/**
 * Created by Amulya on 14/10/2016.
 */
var express=require('express');
var app=express();
const PORT=3020;
app.use(express.static(__dirname+'/../Client'));

app.get('/',function(req,res){
    res.redirect('/index.html');
});

//this will call the 501 handler
app.use('/ServerError',function(req,res){
    throw new Error("blaj");
});

app.use(function(req,res){
    res.redirect('/404.html');
});

app.use(function(err,req,res,next){
    if(err)
        res.send({status:'501',message:'server error'});

});
app.listen(PORT, function(){
    console.log("Server is listening on Port: "+PORT);
});