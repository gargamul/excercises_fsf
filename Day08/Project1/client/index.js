/**
 * Created by Amulya on 19/10/2016.
 */

//to keep everything safe inside a closed scope. because JS is very polluted
(function() {

    var RegApp=angular.module("MyApp",[]);
    var MyCtrl = function () {
        var vm = this;
        vm.item="";
        vm.items=[];

        vm.addToCart=function(){
            vm.items.push(vm.item);
            vm.item='';
        };

        vm.delete=function(index){
            vm.items.splice(index,1);
        }

    };//close of controller
    RegApp.controller("MyCtrl",[MyCtrl]);
})();