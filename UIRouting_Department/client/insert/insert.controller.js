(function () {
    'use strict';
    
    angular
        .module("DepartmentApp")
        .controller("InsertCtrl", InsertCtrl);

    InsertCtrl.$inject = ["$http", "$filter"];

    function InsertCtrl($http, $filter) {
        console.log("-- insert.controller.js");
        
        // Exposed variables
        var vm = this;
        vm.department = {};
        vm.status = {
            message: "",
            success: ""
        };

        // Exposed functions
        vm.register = register;

        // Initial calls
        initEmployees();

        // Function definition
        function initEmployees(){
            console.log("-- insert.controller.js > initEmployees()");
            $http.get("/api/employees")
                .then(function (response) {
                    console.info("-- insert.controller.js > initEmployees then() \n" + JSON.stringify(response.data));
                    vm.employees = response.data;
                })
                .catch(function (err) {
                    console.log("--  insert.controller.js > initEmployees > error: \n" + JSON.stringify(err));
                });
        }
        function register() {
            // We need to add selected employee's start and end date as department manager
            vm.department.from_date = $filter('date')(new Date(), 'yyyy-MM-dd');
            vm.department.to_date = new Date('9999-01-01');
            console.log("vm.dept" + JSON.stringify(vm.department));
            $http.post("/api/departments", vm.department)
                .then(function (response) {
                    console.info("Success! Response returned: " + JSON.stringify(response.data));
                    vm.status.message = "The department is added to the database.";
                    vm.status.success = "ok";
                    console.info("status: " + JSON.stringify(vm.status));
                })
                .catch(function (err) {
                    console.info("Error: " + JSON.stringify(err));
                    vm.status.message = "Failed to add the department to the database.";
                    vm.status.success = "error";
                });
        }
    } // END InsertCtrl

})();






