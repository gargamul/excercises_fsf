/**
 * Created by Amulya on 17/10/2016.
 */
(function(){
    angular
        .module("App")
        .controller("AppCtrl", AppCtrl);

    AppCtrl.$inject = [];
    function AppCtrl(){
        var vm = this;

        console.log("hi from controller");


        vm.submit = function () {
            alert("Submitted");
            console.log("Hello I seem to have written everything");
        };
    } // END AppCtrl

})();
