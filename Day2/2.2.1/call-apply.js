/**
 * Created by Amulya on 12/10/2016.
 */

//this actually works like a constructor. Therefore we can actually just write a constructor function to get this thing working
var obj={
    name:"Amulya",
    say: function(prefix){
        console.log(prefix);
        //console.log(prefix[1]);
        //console.log(this.name);
        return prefix+ this.name;
    }
}
var next={name:"Aiel"};
//console.log(obj.say.call(next,"Hello"));
console.log(obj.say.apply(next,["Hello","baby"]));


//An example of a constructor function
function fruits(name,color,benefits){
    name="Apple";
}