//Load express
var express = require("express");
//Create an instance of express application
var app = express();

var bodyParser = require("body-parser");

//our databases--
//TODO : Import Sequelize <Here>
var Sequelize=require("sequelize");
var conn= new Sequelize('employees','root','Sangro143',{
    host:"localhost",
    dialect: "mysql",
    pool:{
        max:4,
        min:0,
        idle:10000
    }
});

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

/* Serve files from public directory
 __dirname is the absolute path of
 the application directory
 */

app.use(express.static(__dirname + "/../client/"));

//TODO :Create a connection with the MySQL DB here
var Department = require('./model/department')(conn, Sequelize);
var Manager = require('./model/manager')(conn, Sequelize);
 
/*var Department = require('./model/department')(sequelize, Sequelize);
var Manager = require('./model/manager')(sequelize, Sequelize);*/

//TODO : Create table relationships of manager and departments
//creation of foreign_key
Department.hasOne(Manager,{foreignKey : 'dept_no'});

//TODO : Create route handlers to execute sequelize queries.
app.post('/api/departments/',function(req,res){
    console.log("Query", req.query);
    console.log("Body", req.body);

    Department
        .create({
            dept_no: req.body.dept_no,
            dept_name: req.body.dept_name
        })
        .then(function(department){
            res.status(200).json(department);
        })
        .catch(function(error){
            console.log(error);
            res
                .status(500)
                .json({error:true});
        })
});

//TODO: Read query string parameters
//e.g. app.get(....)

app.get('/api/departments', function(req,res){
    var where ={};
    if(req.query.dept_name){
        where.dept_name={ $like: "%"+req.query.dept_name+"%"}
    }

    if(req.query.dept_no){
        where.dept_no= req.query.dept_no;
    }

    console.log(where);
    Department.
        findAll ({
            where: where,
            include: [Manager]
    })
        .then(function(departments){
            res.json(departments);
        })
        .catch(function(){
            res.status(500)
                .json({error:true});
        });
});



//Start the web server on port 3000
app.listen(3000, function () {
    console.info("Webserver started on port 3000");
});
