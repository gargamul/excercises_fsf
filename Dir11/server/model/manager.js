//Create a model for dept_manager table
module.exports = function (conn, Sequelize) {
    var Manager= conn.define("dept_manager",{
        emp_no: {
            type: Sequelize.INTEGER,
            primaryKey : true
        },
        dept_no: {
            type: Sequelize.STRING,
            primaryKey: true
        },
        from_date: {
            type: Sequelize.DATE
        },
        to_date: {
            type: Sequelize.DATE
        }
    },{
        timestamps: false
    });

    return Manager;

};

