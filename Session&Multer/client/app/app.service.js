(function () {
    angular
        .module("UploadApp")
        .service("UploadService", UploadService);

    UploadService.$inject = ["$http", "$q", "Upload"];

    function UploadService($http, $q, Upload) {

        var vm = this;

        vm.refreshAlbum = refresh;

        function refresh() {
            var defer = $q.defer();
            $http.get("/api/album")
                .then(function (results) {
                    defer.resolve(results.data);
                });
            return (defer.promise);
        }

    }
})();
