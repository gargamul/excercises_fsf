/**
 * Created by Amulya on 17/10/2016.
 */
var express=require('express');
var app=express();

const PORT=8081;

app.use(express.static(__dirname+"/../Client/"));

app.get('/find',function(req,res){
    res.redirect('index.html');
});


app.listen(PORT, function(){
    console.log('Listening on port: ' + PORT);
});