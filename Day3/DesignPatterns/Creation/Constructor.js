/**
 * Created by Amulya on 12/10/2016.
 */

//3 different ways of constructor pattern

//1 method
var obj={};
console.log(obj);

//2 method
var obj2=Object.create(Object.prototype);
obj2.name="Alex";
console.log(obj2);

//3 method
var obj3=new Object();
obj3.name="Sandy";
console.log(obj3);

//4 method - to control the object properties..
//Object.defineProperty(obj, prop, descriptor) --> prop is what we are going to change
var obj4=Object.defineProperty(obj3,"name",{
                                            value:"Phang",
                                            writable:false,
                                            ennumerable:false,
                                            configurable:true});
console.log(Object.getOwnPropertyNames(obj4));

/* to access the name property of the variable-- can use
1) obj.name;
2)obj["name"];
3)obj.pop()?
4)obj.get(1);??
 */