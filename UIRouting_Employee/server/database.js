var Sequelize = require("sequelize");
var config = require('./config');

//Create a connection with mysql DB
var sequelize = new Sequelize(
    config.mysql.database,
    config.mysql.username,
    config.mysql.password,
    {
        host: config.mysql.host,
        dialect: 'mysql',

        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    });

sequelize.sync().then(function () {
    console.log("Database in Sync Now")
});

var EmployeeModal = require('./api/employee/employee.modal')(sequelize, Sequelize);
var SalaryModal=require('./api/salary/salary.modal')(sequelize, Sequelize);

EmployeeModal.hasMany(SalaryModal, {foreignKey: 'emp_no'});

module.exports={
    Employees : EmployeeModal,
    Salary : SalaryModal
}