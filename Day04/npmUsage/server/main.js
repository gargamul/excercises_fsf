
// Defines a port to listen to
const PORT = 3000;

var app=require('express');
app=app();


// A function that handles requests and sends a response
// Since no path was specified, this function would handle all client requests
app.use(function (req, res) {
    res.send('It Works!! Path Hit: ' + req.originalUrl);
    console.log(req.query); // to fetch an object from the req
});

// Starts the server on localhost (default)
app.listen(PORT, function () {
    console.log('Server listening on: http://localhost:%s', PORT);
});