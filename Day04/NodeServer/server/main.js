// Port that we'll listen to
const PORT = process.argv[2] || 8080;

var http = require('http');

// Create server
var server = http.createServer(handleRequest);

// Function which handles requests and sends responses
function handleRequest(request, response){

    response.end('It Works!! cool! Path Hit: ' + request.url);
}

// Start server
server.listen(PORT, function(){
    //Callback triggered when server is successfully listening. Hurray!
    console.log("Server listening on: http://localhost:%s", PORT);
});