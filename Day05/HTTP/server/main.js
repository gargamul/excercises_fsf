var express = require('express');
var app=express();

const PORT=8000;

app.use(express.static(__dirname+"/../public"));

//implementation of 5 different kinds of http requests

app.get('/Requesting',function(req,res){

    var responseObj={
        RequestHost : req.method,
        RequestProtocol : req.protocol,
        RequestHostname : req.hostname,
        RequestPath : req.path,
        RequestQuery : req.query,
        RequestCookies : req.cookies,
        RequestIP:req.ip
    };
    console.log("The request details are:\n"+responseObj);
    //create a JSON as a response here
    console.log("Updating IP -->"+req.method);

    res.JSON(responseObj);
});

//this will call the 501 handler
app.use('/ServerError',function(req,res){
    throw new Error("blaj");
});

app.use(function(req,res){
    res.redirect('/404.html');
});

app.use(function(err,req,res,next){
    if(err)
        res.send({status:'501',message:'server error'});

});

//handling different kinds of routing --works
app.route('/')
    .post(function(req,res){
        res.send("Posting done");
    })
    .delete(function(req,res){
        res.send("I swear I have deleted the file");
    })
    .put(function(req,res){
        res.send("Ok updated with put!");
    });

app.listen(PORT, function(){
    console.log("Server is listening on PORT: "+PORT);
});