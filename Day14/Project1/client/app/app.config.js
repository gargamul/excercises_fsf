(function () {
    angular
        .module("UploadApp")
        .config(UploadConfig);

    UploadConfig.$inject = ['$stateProvider'];

    function UploadConfig($stateProvider) {
        $stateProvider
            .state("index", {
                url: "",
                views: {
                    "upload": {
                        templateUrl: "app/upload/upload.view.html",
                        controller: "UploadCtrl",
                        controllerAs: "uploadCtrl"
                    }
                }
            });
    }
})();
