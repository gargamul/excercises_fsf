(function () {
    angular
        .module("DepartmentApp")
        .controller("SearchCtrl", SearchCtrl);

    SearchCtrl.$inject = ["$http"];

    function SearchCtrl($http) {
        var vm = this;
        vm.result={
            dept_name : ''
        };
        vm.editorEnabled = false;
        vm.editableDeptName = "";

        // My search logic starts here
        vm.searchForm={
            dept_name : ''
        };

        vm.departments=[]; // to store the response
        vm.search=function(){
            $http.get('/api/departments',{
                params: vm.searchForm
            })
                .then(function(response){
                    console.log(response.data);
                    vm.departments=response.data;
                })
                .catch(function(){
                    console.log("error- did not get the info from DB");
                })
        };

        vm.enableEditor = function () {
            vm.editorEnabled = true;
            vm.editableDeptName = vm.result.dept_name;
        };

        vm.disableEditor = function () {
            vm.editorEnabled = false;
        };

        vm.save = function () {
            vm.result.dept_name = vm.editableDeptName;

            vm.disableEditor();
        };

        vm.deleteEmpolyeeNo = function () {

        }
    };
})();






